﻿using System;

namespace in60seconds.Modules.DataSystem.Model.VO
{
    public enum RequestLoadDataType
    {
        RESOURCES,
        WWW
    }

    public class RequestLoadDataVO
    {
        public string path;
        public RequestLoadDataType requestType;
        public Type dataType;
    }
}
