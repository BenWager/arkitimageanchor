﻿using System.Collections.Generic;

namespace in60seconds.Modules.DataSystem.Model.VO
{
    public class DataSystemResultDictionaryVO : IDataSystemResult
    {
        public Dictionary<string, object> data;
    }
}
