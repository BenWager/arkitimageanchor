﻿using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.DataSystem.Model.VO;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using in60seconds.Modules.DataSystem.Controller.Notes;
using System.Collections;
using in60seconds.Core.Model.VO;
using in60seconds.Core.Controller.Notes;

namespace in60seconds.Modules.DataSystem.Controller.Commands
{
    public class RequestLoadDataCommand : SimpleCommand
    {
        protected RequestLoadDataVO requestLoadDataVO;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("RequestLoadDataCommand::Execute");

            // Store the request load data VO
            requestLoadDataVO = notification.Body as RequestLoadDataVO;

            // Check if request is for Resources or WWW data
            if(requestLoadDataVO.requestType == RequestLoadDataType.RESOURCES)
            {
                LoadDataFromResources(requestLoadDataVO.path, requestLoadDataVO.dataType);
            }
            else
            {
                LoadDataFromWWW(requestLoadDataVO.path, requestLoadDataVO.dataType);
            }
        }

        /// <summary>
        /// Specify a data type to convert data and return object of that type
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dataType"></param>
        private void LoadDataFromResources(string path, Type dataType)
        {
            if (dataType == null)
            {
                LoadDataFromResources(path);
                return;
            }

            // Get the Raw data as a TextAsset
            TextAsset rawData = Resources.Load<TextAsset>(path.Replace(".json", ""));

            if(rawData == null)
            {
                DebugLogger.LogWarning("RequestLoadDataCommand::LoadDataFromResources -> Could not process application data file");
                return;
            }

            // Deserialize to the specified object
            var data = JsonConvert.DeserializeObject(rawData.text, dataType);

            // Notify system, passing newly created data object as body
            SendNotification(DataSystemNote.DATA_LOADED, data);
        }

        /// <summary>
        /// No data type specified will result in a Dictionary (string,object)
        /// </summary>
        /// <param name="path"></param>
        private void LoadDataFromResources(string path)
        {
            TextAsset rawData = Resources.Load<TextAsset>(path.Replace(".json", ""));

            if (rawData == null)
            {
                DebugLogger.LogWarning("RequestLoadDataCommand::LoadDataFromResources -> Could not process application data file");
                return;
            }


            var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(rawData.text);

            DataSystemResultDictionaryVO dataSystemResultDictionaryVO = new DataSystemResultDictionaryVO();
            dataSystemResultDictionaryVO.data = data;

            SendNotification(DataSystemNote.DATA_LOADED, dataSystemResultDictionaryVO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dataType"></param>
        private void LoadDataFromWWW(string path, Type dataType)
        {
            if (dataType == null)
            {
                LoadDataFromWWW(path);
                return;
            }
            RequestStartCoroutineVO requestStartCoroutineVO = new RequestStartCoroutineVO();
            requestStartCoroutineVO.coroutine = DoLoadDataFromWWW(path, dataType);
            SendNotification(CoreNote.REQUEST_START_COROUTINE, requestStartCoroutineVO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        private void LoadDataFromWWW(string path)
        {
            RequestStartCoroutineVO requestStartCoroutineVO = new RequestStartCoroutineVO();
            requestStartCoroutineVO.coroutine = DoLoadDataFromWWW(path);
            SendNotification(CoreNote.REQUEST_START_COROUTINE, requestStartCoroutineVO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        private IEnumerator DoLoadDataFromWWW(string path, Type dataType)
        {
            WWW www = new WWW(path);

            yield return www;

            var data = JsonConvert.DeserializeObject(www.text, dataType);

            SendNotification(DataSystemNote.DATA_LOADED, data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private IEnumerator DoLoadDataFromWWW(string path)
        {
            WWW www = new WWW(path);

            yield return www;

            var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(www.text);

            DataSystemResultDictionaryVO dataSystemResultDictionaryVO = new DataSystemResultDictionaryVO();
            dataSystemResultDictionaryVO.data = data;

            SendNotification(DataSystemNote.DATA_LOADED, dataSystemResultDictionaryVO);
        }
    }
}
