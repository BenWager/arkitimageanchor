﻿using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.DataSystem.Controller.Notes;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Modules.DataSystem.Controller.Commands
{
    public class DataSystemPrepareCommand : SimpleCommand
    {

        public override void Execute(INotification notification)
        {
            DebugLogger.Log("DataSystemPrepareCommand::Execute");

            DataSystemNote.Register();
        }
    }
}
