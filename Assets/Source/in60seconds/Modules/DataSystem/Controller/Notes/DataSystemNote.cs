﻿using in60seconds.Core;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.DataSystem.Controller.Commands;

namespace in60seconds.Modules.DataSystem.Controller.Notes
{
    public class DataSystemNote
    {
        public const string REQUEST_LOAD_DATA = "dataSystem_requestLoadData";

        public const string DATA_LOADED = "dataSystem_dataLoaded";

        public static void Register()
        {
            DebugLogger.Log("DataSystemNote::Register");

            CoreFacade.GetInstance().RegisterCommand(REQUEST_LOAD_DATA, typeof(RequestLoadDataCommand));
        }
    }
}
