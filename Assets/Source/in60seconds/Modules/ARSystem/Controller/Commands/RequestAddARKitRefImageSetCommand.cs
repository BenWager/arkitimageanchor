﻿using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace in60seconds.Modules.ARSystem.Controller.Commands
{
    public class RequestAddARKitRefImageSetCommand : SimpleCommand
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("RequestAddARKitRefImageSetCommand::Execute");

            AddImageReferenceSet(notification.Body as ARReferenceImagesSet);
        }

        private void AddImageReferenceSet(ARReferenceImagesSet referenceSet)
        {
            UnityARCameraManager cameraManager = GameObject.FindObjectOfType<UnityARCameraManager>();

            cameraManager.detectionImages = referenceSet;

        }
    }
}