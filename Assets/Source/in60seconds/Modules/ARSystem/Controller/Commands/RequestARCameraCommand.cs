﻿using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace in60seconds.Modules.ARSystem.Controller.Commands
{
    public class RequestARCameraCommand : SimpleCommand
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("RequestARCameraCommand::Execute");

            InitARCamera();
        }

        private void InitCamera()
        {
        }

#if UNITY_IOS

        private void InitARCamera()
        {
            
            GameObject gameObject = new GameObject("Camera");
            Camera camera = gameObject.AddComponent<Camera>();
            camera.clearFlags = CameraClearFlags.Depth;
            camera.nearClipPlane = 0.001f;

            GameObject managerGameObject = new GameObject("ARCameraManager");

            UnityARVideo unityARVideo = gameObject.AddComponent<UnityARVideo>();
            unityARVideo.m_ClearMaterial = Resources.Load("ARSystem/YUVMaterial") as Material;
            gameObject.AddComponent<UnityARCameraNearFar>();

            UnityARCameraManager manager = managerGameObject.AddComponent<UnityARCameraManager>();
            manager.SetCamera(camera);
            manager.startAlignment = UnityARAlignment.UnityARAlignmentGravity;
            manager.planeDetection = UnityARPlaneDetection.None;
            manager.getPointCloud = true;
            manager.enableLightEstimation = true;
            manager.enableAutoFocus = true;
            manager.environmentTexturing = UnityAREnvironmentTexturing.UnityAREnvironmentTexturingNone;
            manager.maximumNumberOfTrackedImages = 2;
        }

#elif UNITY_ANDROID

        private void InitARCamera()
        {
            GameObject.Instantiate ( Resources.Load("ARSystem/ARCore Device") as GameObject);
        }

#else

        private void InitARCamera()
        {
            GameObject gameObject = new GameObject("Camera");
            Camera camera = gameObject.AddComponent<Camera>();
            camera.clearFlags = CameraClearFlags.Depth;
            camera.nearClipPlane = 0.001f;


        }

#endif

    }
}
