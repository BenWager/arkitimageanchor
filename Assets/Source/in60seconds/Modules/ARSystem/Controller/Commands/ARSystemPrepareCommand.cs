﻿using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.ARSystem.Controller.Notes;
using in60seconds.Modules.ARSystem.View.Mediators;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Modules.ARSystem.Controller.Commands
{
    public class ARSystemPrepareCommand : SimpleCommand
    {
        
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("DataSystemPrepareCommand::Execute");

            Facade.RegisterMediator(new ARSystemMediator());

            ARSystemNote.Register();
        }
    }
}
