﻿using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using UnityEngine;

namespace in60seconds.Modules.ARSystem.Controller.Commands
{
    public class RequestARLightCommand : SimpleCommand
    {
        private GameObject gameObject;
        private Light light;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("RequestARLightCommand::Execute");

            InitLight();
            InitARLight();
        }

        private void InitLight()
        {
            gameObject = new GameObject("Directional Light");
            light = gameObject.AddComponent<Light>();
            light.type = LightType.Directional;
        }

#if UNITY_IOS

        private void InitARLight()
        {
            gameObject.AddComponent<UnityEngine.XR.iOS.UnityARAmbient>();
        }

#elif UNITY_ANDROID

        private void InitARLight()
        {
            GameObject.Instantiate(Resources.Load("ARSystem/ARCore Environmental Light") as GameObject);
        }

#else

        private void InitARLight()
        {
            
        }

#endif
    
    }
}