﻿using in60seconds.Core;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.ARSystem.Controller.Commands;

namespace in60seconds.Modules.ARSystem.Controller.Notes
{
    public class ARSystemNote
    {
        public const string REQUEST_AR_CAMERA                   = "arSystem_requestARCamera";
        public const string REQUEST_AR_LIGHT                    = "arSystem_dataLoaded";
        public const string REQUEST_ADD_ARKIT_REF_IMAGE_SET     = "arSystem_requestAddARKitRefImageSet";
        public const string REQUEST_ADD_ARCORE_REF_IMAGE_SET    = "arSystem_requestAddARCoreRefImageSet";

        // Detection events
        public const string AR_IMAGE_ANCHOR_DETECTED            = "arSystem_arImageAnchorDetected";
        public const string AR_IMAGE_ANCHOR_UPDATED             = "arSystem_arImageAnchorUpdated";
        public const string AR_IMAGE_ANCHOR_LOST                = "arSystem_arImageAnchorLost";


        public static void Register()
        {
            DebugLogger.Log("ARSystemNote::Register");

            CoreFacade.GetInstance().RegisterCommand(REQUEST_AR_CAMERA, typeof(RequestARCameraCommand));
            CoreFacade.GetInstance().RegisterCommand(REQUEST_AR_LIGHT, typeof(RequestARLightCommand));
            CoreFacade.GetInstance().RegisterCommand(REQUEST_ADD_ARKIT_REF_IMAGE_SET, typeof(RequestAddARKitRefImageSetCommand));
            CoreFacade.GetInstance().RegisterCommand(REQUEST_ADD_ARCORE_REF_IMAGE_SET, typeof(RequestAddARCoreRefImageSetCommand));
        }
    }
}
