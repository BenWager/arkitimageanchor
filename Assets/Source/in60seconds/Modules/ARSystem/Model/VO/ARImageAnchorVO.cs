﻿
using UnityEngine;

namespace in60seconds.Modules.ARSystem.Model.VO
{
    public class ARImageAnchorVO
    {
        public string name;
        public string identifier;
        public Vector3 position;
        public Quaternion rotation;
        public bool isTracked;
        public GameObject prefab;
        public URLButton[] buttons;

        // The instatiated prefab
        public GameObject gameObject;
    }
}
