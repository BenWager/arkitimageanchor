﻿//
// This mediator is used to create 
//

using in60seconds.Application.Controller.Notes;
using in60seconds.Application.Model.Enums;
using in60seconds.Application.Model.Proxies;
using in60seconds.Application.Model.VO;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Core.View.Mediators;
using in60seconds.Modules.ARSystem.Controller.Notes;
using in60seconds.Modules.ARSystem.View.Components;
using PureMVC.Interfaces;
using System.Collections.Generic;
using UnityEngine;

namespace in60seconds.Modules.ARSystem.View.Mediators
{
    public class ARSystemMediator : CoreBaseMediator
    {
        new public const string NAME = "ARSystemMediator";

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Application.View.Mediators.ApplicationMediator"/> class.
        /// </summary>
        public ARSystemMediator() : base(NAME) { }

        /// <summary>
        /// Gets the list notification interests.
        /// </summary>
        /// <value>The list notification interests.</value>
        public override IEnumerable<string> ListNotificationInterests
        {
            get
            {
                return new List<string>(base.ListNotificationInterests)
                {
                    ApplicationNote.APPLICATION_READY
                };
            }
        }


        /// <summary>
        /// Handles the notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public override void HandleNotification(INotification notification)
        {
            base.HandleNotification(notification);

            switch (notification.Name)
            {
                case ApplicationNote.APPLICATION_READY:
                    OnApplicationReady();

                    break;

                default: break;
            }
        }

        /// <summary>
        /// Core Framework is ready
        /// </summary>
        protected override void OnCoreReady()
        {
            base.OnCoreReady();
        }


        /// <summary>
        /// Called once application data has been loaded
        /// </summary>
        protected void OnApplicationReady()
        {
            DebugLogger.Log(NAME + "::OnApplicationReady");

            AddARImageAnchorComponent();

        }

#if UNITY_IOS

        private void AddARImageAnchorComponent()
        {
            // Add an ARSystem GameObject
            GameObject arSystemGameObject = new GameObject("ARSystem");

            // Add an ARImageAnchorComponent
            ARKitImageAnchorComponent imageAnchorComponent = arSystemGameObject.AddComponent<ARKitImageAnchorComponent>();

            // Add listeners to dispatch ar image track events
            imageAnchorComponent.OnImageAnchorDetected += ImageAnchorComponent_OnImageAnchorDetected;
            imageAnchorComponent.OnImageAnchorUpdated += ImageAnchorComponent_OnImageAnchorUpdated;
            imageAnchorComponent.OnImageAnchorLost += ImageAnchorComponent_OnImageAnchorLost;
        }

#elif UNITY_ANDROID

        private void AddARImageAnchorComponent()
        {
            // Add an ARSystem GameObject
            GameObject arSystemGameObject = new GameObject("ARSystem");

            ARCoreImageAnchorComponent imageAnchorComponent = arSystemGameObject.AddComponent<ARCoreImageAnchorComponent>();

            // Add listeners to dispatch ar image track events
            imageAnchorComponent.OnImageAnchorDetected += ImageAnchorComponent_OnImageAnchorDetected;
            imageAnchorComponent.OnImageAnchorUpdated += ImageAnchorComponent_OnImageAnchorUpdated;
            imageAnchorComponent.OnImageAnchorLost += ImageAnchorComponent_OnImageAnchorLost;

        }

#else
        private void AddARImageAnchorComponent()
        {

        }

#endif
        private void ImageAnchorComponent_OnImageAnchorDetected(Model.VO.ARImageAnchorVO arImageAnchorVO)
        {
            SendNotification(ARSystemNote.AR_IMAGE_ANCHOR_DETECTED, arImageAnchorVO);
        }

        private void ImageAnchorComponent_OnImageAnchorUpdated(Model.VO.ARImageAnchorVO arImageAnchorVO)
        {
            SendNotification(ARSystemNote.AR_IMAGE_ANCHOR_UPDATED, arImageAnchorVO);
        }

        private void ImageAnchorComponent_OnImageAnchorLost(Model.VO.ARImageAnchorVO arImageAnchorVO)
        {
            SendNotification(ARSystemNote.AR_IMAGE_ANCHOR_LOST, arImageAnchorVO);
        }
    }
}