﻿using in60seconds.Modules.ARSystem.Model.VO;
using UnityEngine;
using System.Collections.Generic;
using GoogleARCore;
using System.Linq;

namespace in60seconds.Modules.ARSystem.View.Components
{
    public class ARCoreImageAnchorComponent : MonoBehaviour
    {
        public delegate void ImageAnchorEvent(ARImageAnchorVO arImageAnchorVO);
        public event ImageAnchorEvent OnImageAnchorDetected;
        public event ImageAnchorEvent OnImageAnchorUpdated;
        public event ImageAnchorEvent OnImageAnchorLost;

        private List<string> m_TrackedImages = new List<string>();

        private List<AugmentedImage> m_AugmentedImages = new List<AugmentedImage>();
        private List<AugmentedImage> m_TempAugmentedImages = new List<AugmentedImage>();

        private void Start()
        {
            // Prevent device from going to sleep
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        /// <summary>
        /// The Unity Update method.
        /// </summary>
        public void Update()
        {
            // Check that motion tracking is tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                return;
            }

            // Get updated augmented images for this frame.
            Session.GetTrackables<AugmentedImage>(m_TempAugmentedImages, TrackableQueryFilter.All);

            Dictionary<string, bool> tmp = new Dictionary<string, bool>();
            foreach(string name in m_TrackedImages)
            {
                tmp.Add(name, false);
            }

            // Create visualizers and anchors for updated augmented images that are tracking and do not previously
            // have a visualizer. Remove visualizers for stopped images.
            foreach (var image in m_TempAugmentedImages)
            {
                Debug.Log(image.Name + " : " + image.TrackingState + " - " + image.ExtentX);

                // Check against TrackedImages
                if (image.TrackingState == TrackingState.Tracking && !m_TrackedImages.Contains(image.Name))
                {
                    TrackingStarted(image);
                }
                else if(image.TrackingState == TrackingState.Tracking && m_TrackedImages.Contains(image.Name))
                {
                    TrackingUpdated(image);
                }

                if(image.TrackingState == TrackingState.Stopped || image.TrackingState == TrackingState.Paused) 
                {
                    TrackingStopped(image);
                }

            }

        }

        private void TrackingStarted(AugmentedImage image)
        {
            Debug.Log("TrackingStarted : " + image.Name);;

            m_TrackedImages.Add(image.Name); 

            Anchor anchor = image.CreateAnchor(image.CenterPose);

            ARImageAnchorVO aRImageAnchorVO = new ARImageAnchorVO();
            aRImageAnchorVO.identifier = image.Name;
            aRImageAnchorVO.name = image.Name;
            aRImageAnchorVO.isTracked = true;
            aRImageAnchorVO.position = anchor.transform.position;
            aRImageAnchorVO.rotation = anchor.transform.rotation;

            if(OnImageAnchorDetected != null)
            {
                OnImageAnchorDetected(aRImageAnchorVO);   
            }
        }

        private void TrackingUpdated(AugmentedImage image)
        {
            Debug.Log("Tracking Updated : " + image.Name);
            Anchor anchor = image.CreateAnchor(image.CenterPose);

            ARImageAnchorVO aRImageAnchorVO = new ARImageAnchorVO();
            aRImageAnchorVO.identifier = image.Name;
            aRImageAnchorVO.name = image.Name;
            aRImageAnchorVO.isTracked = true;
            aRImageAnchorVO.position = anchor.transform.position;
            aRImageAnchorVO.rotation = anchor.transform.rotation;

            if (OnImageAnchorUpdated != null)
            {
                OnImageAnchorUpdated(aRImageAnchorVO);
            }
        }

        private void TrackingStopped(AugmentedImage image)
        {
            Debug.Log("Tracking Stopped : " + image.Name);

            m_TrackedImages.Remove(image.Name);

            ARImageAnchorVO aRImageAnchorVO = new ARImageAnchorVO();
            aRImageAnchorVO.identifier = image.Name;
            aRImageAnchorVO.name = image.Name;
            aRImageAnchorVO.isTracked = false;

            if (OnImageAnchorLost != null)
            {
                OnImageAnchorLost(aRImageAnchorVO);
            }

            
        }
    }
}