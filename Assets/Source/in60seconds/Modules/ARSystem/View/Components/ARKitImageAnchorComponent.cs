﻿using in60seconds.Modules.ARSystem.Model.VO;
using UnityEngine;
using UnityEngine.XR.iOS;

namespace in60seconds.Modules.ARSystem.View.Components
{
    public class ARKitImageAnchorComponent : MonoBehaviour
    {
        public delegate void ImageAnchorEvent(ARImageAnchorVO arImageAnchorVO);
        public event ImageAnchorEvent OnImageAnchorDetected;
        public event ImageAnchorEvent OnImageAnchorUpdated;
        public event ImageAnchorEvent OnImageAnchorLost;


        // Use this for initialization
        void Start()
        {
            UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;
        }

        void AddImageAnchor(ARImageAnchor arImageAnchor)
        {
            Debug.LogFormat("image anchor added[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

            ARImageAnchorVO arImageAnchorVO = new ARImageAnchorVO();
            arImageAnchorVO.name = arImageAnchor.referenceImageName;
            arImageAnchorVO.identifier = arImageAnchor.identifier;
            arImageAnchorVO.position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
            arImageAnchorVO.rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);
            arImageAnchorVO.isTracked = arImageAnchor.isTracked;

            // Get the corresponding ARReferenceImageObject
            UnityARCameraManager cameraManager = FindObjectOfType<UnityARCameraManager>();
            ARReferenceImagesSet detectionImages = cameraManager.detectionImages;

            foreach (ARReferenceImage referenceImage in detectionImages.referenceImages)
            {
                if (referenceImage.imageName == arImageAnchor.referenceImageName)
                {
                    arImageAnchorVO.prefab = referenceImage.prefab;
                    arImageAnchorVO.buttons = referenceImage.buttons;
                    break;
                }
            }

            if (OnImageAnchorDetected != null)
                OnImageAnchorDetected(arImageAnchorVO);
        }

        void UpdateImageAnchor(ARImageAnchor arImageAnchor)
        {
            Debug.LogFormat("image anchor updated[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

            ARImageAnchorVO arImageAnchorVO = new ARImageAnchorVO();
            arImageAnchorVO.name = arImageAnchor.referenceImageName;
            arImageAnchorVO.identifier = arImageAnchor.identifier;
            arImageAnchorVO.position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
            arImageAnchorVO.rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);
            arImageAnchorVO.isTracked = arImageAnchor.isTracked;

            if (OnImageAnchorUpdated != null)
                OnImageAnchorUpdated(arImageAnchorVO);
        }

        void RemoveImageAnchor(ARImageAnchor arImageAnchor)
        {
            Debug.LogFormat("image anchor removed[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

            ARImageAnchorVO arImageAnchorVO = new ARImageAnchorVO();
            arImageAnchorVO.name = arImageAnchor.referenceImageName;
            arImageAnchorVO.identifier = arImageAnchor.identifier;
            arImageAnchorVO.position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
            arImageAnchorVO.rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);
            arImageAnchorVO.isTracked = arImageAnchor.isTracked;

            if (OnImageAnchorLost != null)
                OnImageAnchorLost(arImageAnchorVO);
        }

        void OnDestroy()
        {
            UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
            UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;
        }
    }
}