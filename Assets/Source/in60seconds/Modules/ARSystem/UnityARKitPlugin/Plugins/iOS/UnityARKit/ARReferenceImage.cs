﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class URLButton
{
    public GameObject prefab;
    public string label;
    public string url;
}

[CreateAssetMenu(fileName = "ARReferenceImage" , menuName = "UnityARKitPlugin/ARReferenceImage", order = 2)]
public class ARReferenceImage : ScriptableObject {

	public string imageName;
	public Texture2D imageTexture;
	public float physicalSize;
    public GameObject prefab;
    [SerializeField]
    public URLButton[] buttons;

}