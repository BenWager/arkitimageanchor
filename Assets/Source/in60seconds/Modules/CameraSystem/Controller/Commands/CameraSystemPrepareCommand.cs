﻿using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.CameraSystem.View.Mediators;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Modules.CameraSystem.Controller.Commands
{
    public class CameraSystemPrepareCommand : SimpleCommand
    {

        public override void Execute(INotification notification)
        {
            DebugLogger.Log("CameraSystemPrepareCommand::Execute");
            Facade.RegisterMediator(new CameraSystemMediator());
        }
    }
}