﻿namespace in60seconds.Modules.CameraSystem.Controller.Notes
{
    public class CameraSystemNote
    {
        public const string REQUEST_ADD_CAMERA = "cameraSystem_requestAddCamera";

        public const string REQUEST_MOVE_CAMERA = "cameraSystem_requestMoveCamera";

        public const string CAMERA_MOVE_COMPLETE = "cameraSystem_cameraMoveComplete";

        public const string REQUEST_CHANGE_CAMERA_FOV = "cameraSystem_requestChangeCameraFOV";
    }
}
