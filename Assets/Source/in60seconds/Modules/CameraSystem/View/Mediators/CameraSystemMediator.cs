﻿using in60seconds.Core.View.Mediators;
using UnityEngine;
using in60seconds.Modules.CameraSystem.View.Components;
using System.Collections.Generic;
using PureMVC.Interfaces;
using in60seconds.Modules.CameraSystem.Controller.Notes;
using in60seconds.Modules.CameraSystem.Model.VO;
using in60seconds.Modules.CameraSystem.Model.Enums;
using UnityEngine.Events;

namespace in60seconds.Modules.CameraSystem.View.Mediators
{
    public class CameraSystemMediator : CoreBaseMediator
    {
        protected static new string NAME = "CameraSystemMediator";

        protected CameraSystemComponent cameraSystemComponent;

        protected List<Behaviour> queuedToEnable;

        protected UnityAction callback;

        static Camera backgroundCam;

        /// <summary>
        /// 
        /// </summary>
        public CameraSystemMediator() : base(NAME) { }

        /// <summary>
        /// 
        /// </summary>
        public override void OnRegister()
        {
            base.OnRegister();

            InstantiateCameraComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InstantiateCameraComponent()
        {
            GameObject gameObject = new GameObject("CameraSystem");
            /*
            Camera cam = gameObject.AddComponent<Camera>();
            cam.clearFlags = CameraClearFlags.SolidColor;
            cam.backgroundColor = new Color32 (224, 225, 229, 255);
            cam.nearClipPlane = 0.01f;
            */

            cameraSystemComponent = gameObject.AddComponent<CameraSystemComponent>();
            cameraSystemComponent.OnCameraMovementCompletedEvent += OnCameraMoveComplete;
            GameObject.DontDestroyOnLoad(cameraSystemComponent);

            Debug.Log("CameraSystemMediator::InstantiateCameraComponent -> " + cameraSystemComponent);
        }

        /// <summary>
        /// Gets the list notification interests.
        /// </summary>
        /// <value>The list notification interests.</value>
        public override IEnumerable<string> ListNotificationInterests
        {
            get
            {
                return new List<string>(base.ListNotificationInterests)
                {
                    CameraSystemNote.REQUEST_ADD_CAMERA,
                    CameraSystemNote.REQUEST_MOVE_CAMERA,
                    CameraSystemNote.REQUEST_CHANGE_CAMERA_FOV
                };
            }
        }

        /// <summary>
        /// Handles the notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public override void HandleNotification(INotification notification)
        {
            switch (notification.Name)
            {
                case CameraSystemNote.REQUEST_ADD_CAMERA:
                    OnRequestAddCamera(notification.Body as GameObject);

                    break;
                
                case CameraSystemNote.REQUEST_MOVE_CAMERA:
                    OnRequestMoveCamera(notification.Body as CameraSystemTransitionVO);

                    break;

                case CameraSystemNote.REQUEST_CHANGE_CAMERA_FOV:
                    OnRequestChangeCameraFOV(float.Parse(notification.Body.ToString()));

                    break;

                default: break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cameraGameObject"></param>
        private void OnRequestAddCamera(GameObject cameraGameObject)
        {
            cameraGameObject.transform.SetParent(cameraSystemComponent.transform);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        private void OnRequestChangeCameraFOV(float v)
        {
            foreach(Camera camera in cameraSystemComponent.GetComponentsInChildren<Camera>())
            {
                camera.fieldOfView = v;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cameraSystemTransitionVO"></param>
        private void OnRequestMoveCamera(CameraSystemTransitionVO cameraSystemTransitionVO)
        {
            // Disable any registered behaviours
            foreach(Behaviour behaviour in cameraSystemTransitionVO.disableWhileTransitioning)
            {
                behaviour.enabled = false;
            }

            // And get them ready to re enable
            queuedToEnable = cameraSystemTransitionVO.disableWhileTransitioning;

            // camera moved callback
            callback = cameraSystemTransitionVO.callback;

            Camera cam = cameraSystemTransitionVO.destination.GetComponent<Camera>();
            if (cam != null)
            {
                OnRequestChangeCameraFOV(cam.fieldOfView);
            }
            else
            {
                OnRequestChangeCameraFOV(19);
            }

            switch (cameraSystemTransitionVO.easing)
            {
                case CameraSystemEasing.MOVE:
                    cameraSystemComponent.MoveCameraTo(cameraSystemTransitionVO.destination);

                    break;
                case CameraSystemEasing.LERP:
                    cameraSystemComponent.LerpCameraTo(cameraSystemTransitionVO.destination, cameraSystemTransitionVO.duration);

                    break;
                case CameraSystemEasing.SLERP:
                    cameraSystemComponent.SlerpCameraTo(cameraSystemTransitionVO.destination, cameraSystemTransitionVO.duration);

                    break;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private void OnCameraMoveComplete()
        {
            if (callback != null)
            {
                Debug.Log("Attempt callback");
                callback();

            }

            Debug.Log(NAME + "::OnCameraMoveComplete");
            foreach (Behaviour behaviour in queuedToEnable)
            {
                behaviour.enabled = true;
            }

            // Notify system
            SendNotification(CameraSystemNote.CAMERA_MOVE_COMPLETE);
        }
    }
}
