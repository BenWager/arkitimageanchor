﻿using System.Collections;
using UnityEngine;

namespace in60seconds.Modules.CameraSystem.View.Components
{
    public sealed class CameraSystemComponent : MonoBehaviour
    {
        public delegate void CameraMovementCompleted();
        public event CameraMovementCompleted OnCameraMovementCompletedEvent;

        public void MoveCameraTo(Transform destination)
        {
            transform.position = destination.position;
            transform.rotation = destination.rotation;

            if (OnCameraMovementCompletedEvent != null)
            {
                OnCameraMovementCompletedEvent();
            }
        }

        public void LerpCameraTo(Transform destination, float time)
        {
            StopAllCoroutines();
            StartCoroutine(DoLerpCameraTo(destination, time));
        }

        private IEnumerator DoLerpCameraTo(Transform destination, float time)
        {
            float elapsed = 0;
            Transform origin = transform;

            while (elapsed < time)
            {
                elapsed += Time.deltaTime;

                transform.position = Vector3.Lerp(origin.position, destination.position, elapsed / time);
                transform.rotation = Quaternion.Lerp(origin.rotation, destination.rotation, elapsed / time);

                yield return new WaitForEndOfFrame();
            }

            transform.position = destination.position;
            transform.rotation = destination.rotation;

            if(OnCameraMovementCompletedEvent != null)
            {
                OnCameraMovementCompletedEvent();
            }
        }

        public void SlerpCameraTo(Transform destination, float time)
        {
            StopAllCoroutines();
            StartCoroutine(DoSlerpCameraTo(destination, time));
        }

        private IEnumerator DoSlerpCameraTo(Transform destination, float time)
        {
            float elapsed = 0;
            Transform origin = transform;

            while (elapsed < time)
            {
                elapsed += Time.deltaTime;

                transform.position = Vector3.Slerp(origin.position, destination.position, elapsed / time);
                transform.rotation = Quaternion.Slerp(origin.rotation, destination.rotation, elapsed / time);

                yield return new WaitForEndOfFrame();
            }

            transform.position = destination.position;
            transform.rotation = destination.rotation;

            if (OnCameraMovementCompletedEvent != null)
            {
                OnCameraMovementCompletedEvent();
            }
        }

    }
}
