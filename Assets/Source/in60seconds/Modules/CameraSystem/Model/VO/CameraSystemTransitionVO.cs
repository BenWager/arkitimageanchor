﻿using in60seconds.Modules.CameraSystem.Model.Enums;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace in60seconds.Modules.CameraSystem.Model.VO
{
    class CameraSystemTransitionVO
    {
        public Transform destination = null;
        public CameraSystemEasing easing = CameraSystemEasing.LERP;
        public float duration = 1.0f;
        public List<Behaviour> disableWhileTransitioning = new List<Behaviour>();
        public UnityAction callback;
    }
}