﻿namespace in60seconds.Modules.CameraSystem.Model.Enums
{
    public enum CameraSystemEasing
    {
        MOVE,
        LERP,
        SLERP
    }
}
