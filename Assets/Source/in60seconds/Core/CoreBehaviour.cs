﻿/************************************************************************************
 * 																					*
 * CoreBehaviour.cs															
 * 																					*
 * This behaviour should be attached directly to the main Application GameObject.
 * This is used as an entry point for establishing a concrete application facade.	*
 * 																					*
 ************************************************************************************/
namespace in60seconds.Core
{
	using System;
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
	using UnityEngine;

	public class CoreBehaviour : MonoBehaviour
	{
		/// <summary>
		/// Application reset event.
		/// </summary>
		public delegate void CoreResetEvent();
		public event CoreResetEvent OnCoreReset;

        // Set public so it can be set in Editor
        public string DefaultEnvironmentScene = "Environment";

        /// <summary>
        /// Log to console.
        /// </summary>
        public bool debug = true;

		/// <summary>
		/// The core facade.
		/// </summary>
		private CoreFacade coreFacade;

		/// <summary>
		/// Start this instance.
		/// </summary>
		protected virtual void Start()
		{
			DebugLogger.LogTexts = debug;

			try
			{
				coreFacade = new CoreFacade(CoreFacade.DEFAULT_INSTANCE_KEY);
				coreFacade.Startup(this);
			}
			catch (Exception exception)
			{
				throw new UnityException("Unable to initiate coreFacade", exception);
			}
		}

        /// <summary>
        /// Check for key press and fire an Application reset event
        /// </summary>
        protected virtual void Update()
		{
			if (Input.GetKeyDown(KeyCode.R))
			{
				if (OnCoreReset != null)
				{
					//OnCoreReset();
				}
			}

			// Exit application on Escape key press.
			if (Input.GetKeyDown(KeyCode.Escape)) {
            	Application.Quit();
			}
		}

        /// <summary>
        /// On destroy.
        /// </summary>
        protected virtual void OnDestroy()
		{
			if (coreFacade != null)
			{
				coreFacade.Dispose();
				coreFacade = null;
			}
		}
	}
}