﻿namespace in60seconds.Core.Controller.Commands.Request
{
	using in60seconds.Core.Controller.Notes;
	using in60seconds.Core.Model.Proxies;
	using PureMVC.Interfaces;
	using PureMVC.Patterns;

	public class CoreRequestResetCommand : SimpleCommand
	{
		/// <summary>
		/// Reset the the application.
		/// 
		/// Currently we just load the default environment
		/// This command will likely grow as the application does
		/// </summary>
		/// <param name="notification">Notification.</param>
		public override void Execute(INotification notification)
		{
			CoreDataProxy data = (CoreDataProxy)Facade.RetrieveProxy(CoreDataProxy.NAME);

			SendNotification(CoreNote.REQUEST_LOAD_ENVIRONMENT, data.GetDefaultEnvironment());
		}
	}
}