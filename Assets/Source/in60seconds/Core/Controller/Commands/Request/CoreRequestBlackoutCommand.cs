﻿namespace in60seconds.Core.Controller.Commands.Request
{
    using PureMVC.Patterns;
    using PureMVC.Interfaces;
    using in60seconds.Core.Controller.Notes;
    using UnityEngine.SceneManagement;
    using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
    using in60seconds.Core.Model.Proxies;
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;
    using Model.VO;
    using UnityEngine.Events;

    public class CoreRequestBlackoutCommand : SimpleCommand
	{
        /// <summary>
        /// Store a reference to canvasgroup for fading to black
        /// </summary>
        private CanvasGroup canvasGroup;

        private CoreDataProxy coreDataProxy;

		/// <summary>
		/// Execute the specified notification.
		/// </summary>
		/// <param name="notification">Notification.</param>
		public override void Execute(INotification notification)
		{
			DebugLogger.Log("CoreRequestBlackoutCommand::Execute");

            RequestBlackoutVO requestBlackoutVO = notification.Body as RequestBlackoutVO;

            // Get the data proxy to check blackoutstatus
            coreDataProxy = Facade.RetrieveProxy(CoreDataProxy.NAME) as CoreDataProxy;

            if (coreDataProxy.GetBlackoutStatus() != BlackoutStatus.NONE)
            {
                return;
            }

            // First see if blackout canvas already exists in scene
            if(!GameObject.Find("BlackoutCanvas"))
            {
                // Create a canvas to fade to black
                GameObject fadeCanvas = new GameObject("BlackoutCanvas");
                GameObject.DontDestroyOnLoad(fadeCanvas);
                fadeCanvas.AddComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
                canvasGroup = fadeCanvas.AddComponent<CanvasGroup>();

                // Create an image component as child of fadeCanvas
                GameObject image = new GameObject("Image");
                image.transform.SetParent(fadeCanvas.transform);
                Image imageComponent = image.AddComponent<Image>();
                imageComponent.color = Color.black;
                RectTransform rectTransform = image.GetComponent<RectTransform>();
                rectTransform.anchorMin = new Vector2(0, 0);
                rectTransform.anchorMax = new Vector2(1, 1);
                rectTransform.offsetMin = new Vector2(0, 0);
                rectTransform.offsetMax = new Vector2(0, 0);
            }
            else
            {
                canvasGroup = GameObject.Find("BlackoutCanvas").GetComponent<CanvasGroup>();
            }

            // Request a coroutine to fade in blackout canvas
            RequestStartCoroutineVO requestStartCoroutineVO = new RequestStartCoroutineVO();

            // Check fade direction
            if (requestBlackoutVO.fadeDirection == FadeDirection.IN)
            {
                coreDataProxy.SetBlackoutStatus(BlackoutStatus.IN);

                requestStartCoroutineVO.coroutine = DoFadeIn(requestBlackoutVO.delay, requestBlackoutVO.callback);
            }
            else
            {
                coreDataProxy.SetBlackoutStatus(BlackoutStatus.OUT);

                SendNotification(CoreNote.REQUEST_STOP_COROUTINE, DoFadeIn());

                requestStartCoroutineVO.coroutine = DoFadeOut(requestBlackoutVO.delay, requestBlackoutVO.callback);
            }

            SendNotification(CoreNote.REQUEST_START_COROUTINE, requestStartCoroutineVO);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        protected IEnumerator DoFadeIn(float delay = 0, UnityAction callback = null)
        {
            if(delay > 0)
            {
                yield return new WaitForSeconds(delay);
            }

            float time = .3f;
            float elapsed = 0;
            while (elapsed < time)
            {
                if(canvasGroup == null)
                {
                    break;
                }

                canvasGroup.alpha = Mathf.Lerp(0, 1, elapsed / time);

                yield return new WaitForEndOfFrame();

                elapsed += Time.deltaTime;
            }

            if (canvasGroup != null)
            {
                canvasGroup.alpha = 1;
            }

            coreDataProxy.SetBlackoutStatus(BlackoutStatus.NONE);

            if (callback != null)
                callback();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        protected IEnumerator DoFadeOut(float delay = 0, UnityAction callback = null)
        {
            if (delay > 0)
            {
                yield return new WaitForSeconds(delay);
            }

            float time = .6f;
            float elapsed = 0;
            while (elapsed < time)
            {
                if (canvasGroup == null)
                {
                    break;
                }

                canvasGroup.alpha = Mathf.Lerp(1, 0, elapsed / time);

                yield return new WaitForEndOfFrame();

                elapsed += Time.deltaTime;
            }

            if (canvasGroup != null)
            {
                canvasGroup.alpha = 0;
                GameObject.Destroy(canvasGroup.gameObject);
            }

            coreDataProxy.SetBlackoutStatus(BlackoutStatus.NONE);


            if (callback != null)
                callback();

        }
    }
}