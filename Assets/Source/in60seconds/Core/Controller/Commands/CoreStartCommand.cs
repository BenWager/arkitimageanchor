﻿namespace in60seconds.Core.Controller.Commands
{
	using PureMVC.Patterns;
	using in60seconds.Core.Controller.Commands.Prepare;
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
    using System.IO;
    using System.Reflection;
    using System.Linq;
    using System;
    using in60seconds.Application.Controller.Commands;
    using in60seconds.Application.Controller.Commands.Prepare;

    public class CoreStartCommand : MacroCommand
	{
		/// <summary>
		/// Initializes the macro command.
		/// </summary>
		protected override void InitializeMacroCommand()
		{
			DebugLogger.Log("CoreStartCommand::InitializeMacroCommand");

			AddSubCommand(typeof(CoreModelPrepareCommand));
			AddSubCommand(typeof(CoreViewPrepareCommand));
			AddSubCommand(typeof(CoreControllerPrepareCommand));

            // Auto loading modules requires reflection, which is not supported on iOS build target
            // If building for iOS, comment out this method call
            // And instead register modules manually in ApplicationPrepareCommand e.g
            // AddSubCommand(typeof(CameraSystemPrepareCommand));
#if UNITY_IOS
                  DebugLogger.LogWarning("CoreStartCommand::InitializeMacroCommand -> You are building for iOS target. Make sure you register your Modules in ApplicationPrepareCommand!");
#else
            //LoadModules();
#endif

            AddApplicationSubCommand();

            AddSubCommand(typeof(CoreReadyCommand));
		}
        /*
        /// <summary>
        /// 
        /// </summary>
        protected void LoadModules()
        {
            // Check if there are any Modules to load
            string modulesDirectoryPath = Path.Combine(UnityEngine.Application.dataPath, "INDG/Modules");
            DirectoryInfo directoryInfo = new DirectoryInfo(modulesDirectoryPath);
            foreach (DirectoryInfo di in directoryInfo.GetDirectories())
            {
                try
                {
                    // Search for Module prepare command
                    var assembly = Assembly.GetExecutingAssembly();

                    var type = assembly.GetTypes()
                        .First(t => t.Name == di.Name + "PrepareCommand");

                    // If prepare command found, then add as a subcommand
                    if (type != null)
                    {
                        AddSubCommand(type);
                    }
                }
                catch (Exception ex)
                {
                    DebugLogger.LogWarning("Could not find " + di.Name + "PrepareCommand in " + di.Name + " Module : " + ex.Message);
                }
            }
        }
        */

        /// <summary>
        /// Attempt to add subcommands for Model/View/Controller prepare commands of Application codebase
        /// These files MUST exist!
        /// </summary>
        private void AddApplicationSubCommand()
        {
            try
            {
                AddSubCommand(typeof(ApplicationModelPrepareCommand));
            }
            catch (Exception ex)
            {
                DebugLogger.LogError("Error: You must have an ApplicationModelPrepareCommand in INDG.Application.Controller.Commands.Prepare namespace : " + ex.Message);
            }

            try
            {
                AddSubCommand(typeof(ApplicationViewPrepareCommand));
            }
            catch (Exception ex)
            {
                DebugLogger.LogError("Error: You must have an ApplicationViewPrepareCommand in INDG.Application.Controller.Commands.Prepare namespace : " + ex.Message);
            }

            try
            {
                AddSubCommand(typeof(ApplicationControllerPrepareCommand));
            }
            catch (Exception ex)
            {
                DebugLogger.LogError("Error: You must have an ApplicationControllerPrepareCommand in INDG.Application.Controller.Commands.Prepare namespace : " + ex.Message);
            }
        }
    }
}