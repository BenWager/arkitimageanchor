﻿namespace in60seconds.Core.Controller.Commands.Prepare
{
	using PureMVC.Patterns;
	using PureMVC.Interfaces;
	using in60seconds.Core.Model.Proxies;
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;

	public class CoreModelPrepareCommand : SimpleCommand
	{
		/// <summary>
		/// Execute the specified aNote.
		/// </summary>
		/// <param name="aNote">A note.</param>
		public override void Execute(INotification aNote)
		{
			DebugLogger.Log("CoreModelPrepareCommand::Execute");

			Facade.RegisterProxy(new CoreDataProxy());
		}
	}
}