﻿namespace in60seconds.Core.Controller.Commands.Prepare
{
	using PureMVC.Patterns;
	using PureMVC.Interfaces;
	using in60seconds.Core.View.Mediators;
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
    using in60seconds.Application.View.Mediators;
    using System;

    public class CoreViewPrepareCommand : SimpleCommand
	{
		/// <summary>
		/// Prepare view command.
		/// 
		/// The body of this notification is the ApplicationBehaviour
		/// It makes sense to assign the CoreBehaviour as the view component of our ApplicationMediator
		/// </summary>
		public override void Execute(INotification notification)
		{
			DebugLogger.Log("CoreViewPrepareCommand::Execute");

			Facade.RegisterMediator(new CoreMediator(notification.Body as CoreBehaviour));
        }
	}
}