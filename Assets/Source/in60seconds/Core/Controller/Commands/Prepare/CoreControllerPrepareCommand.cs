﻿/***********************************************************************************
 * 
 * PrepareControllerCommand.cs 
 * 
 * Register all commands required on start
 * 
 * Facade.GetInstance().RegisterCommand(CoreNote.NOTE_TO_REGISTER, typeof(NameOfCommand));
 * 
 ***********************************************************************************/

namespace in60seconds.Core.Controller.Commands.Prepare
{
	using PureMVC.Patterns;
	using PureMVC.Interfaces;
	using in60seconds.Core.Controller.Notes;
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
    using in60seconds.Core.Controller.Commands.Request;

    public class CoreControllerPrepareCommand : SimpleCommand
	{
		/// <summary>
		/// Execute the specified aNote.
		/// </summary>
		/// <param name="notification">A note.</param>
		public override void Execute(INotification notification)
		{
			DebugLogger.Log("CoreControllerPrepareCommand::Execute");

			Facade.RegisterCommand(CoreNote.REQUEST_LOAD_ENVIRONMENT, typeof(CoreRequestLoadEnvironmentCommand));
            Facade.RegisterCommand(CoreNote.REQUEST_CORE_RESET, typeof(CoreRequestResetCommand));
            Facade.RegisterCommand(CoreNote.REQUEST_BLACKOUT, typeof(CoreRequestBlackoutCommand));
        }
    }
}