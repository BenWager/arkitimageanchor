﻿namespace in60seconds.Core.Controller.Commands
{
	using PureMVC.Patterns;
	using PureMVC.Interfaces;
	using in60seconds.Core.Controller.Notes;
	using in60seconds.Core.Model.Proxies;
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;

    public class CoreReadyCommand : SimpleCommand
	{
		/// <summary>
		/// Prepare view command.
		/// </summary>
		public override void Execute(INotification notification)
		{
			DebugLogger.Log("CoreReadyCommand::Execute");

			// Retrieve data proxy
			CoreDataProxy coreDataProxy = (Facade.RetrieveProxy(CoreDataProxy.NAME) as CoreDataProxy);

			// Load Default environment
			SendNotification(CoreNote.REQUEST_LOAD_ENVIRONMENT, coreDataProxy.GetDefaultEnvironment());

            // Notify any subscribers that the application is ready
            SendNotification(CoreNote.CORE_READY);
        }
    }
}