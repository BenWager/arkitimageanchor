﻿namespace in60seconds.Core.Controller.Notes
{
	public static class CoreNote
	{
        // Initial Command called from Core Facade to kick start the application
        // This is called internally.
		public const string CORE_START = "core_coreStart";

        // Called after core start, once core mediators have been registered
        // This is called internally. 
		public const string CORE_READY = "core_coreReady";

        // Make a request for the application to reset
		public const string REQUEST_CORE_RESET = "core_requestCoreReset";

        // Make a request to load a new environment scene
		public const string REQUEST_LOAD_ENVIRONMENT = "core_requestLoadEnvironment";

        // Make a request to show / hide blackout canvas
        public const string REQUEST_BLACKOUT = "core_requestBlackout";

        // New environment scene has been successfully loaded
		public const string ENVIRONMENT_LOADED = "core_environmentLoaded";

        // Start a coroutine from outside a MonoBehvaiour - e.q from an PureMVC Command
        public const string REQUEST_START_COROUTINE = "core_requestStartCoroutine";

        public const string REQUEST_STOP_COROUTINE = "core_requestStopCoroutine";

        public const string REQUEST_STOP_ALL_COROUTINES = "core_requestStopAllCoroutines";

        public const string REQUEST_UPDATE_APPLICAITON_TIMESCALE = "core_requestUpdateApplicationTimeScale";
    }
}