﻿using UnityEngine;

namespace in60seconds.Core
{
    public static class CoreExtensions
    {
        /// <summary>
        /// Extension method for GameObject to recursively change the active state of object and children
        /// </summary>
        /// <param name="go"></param>
        /// <param name="active"></param>
        public static void SetActiveRecursive(this GameObject go, bool active)
        {
            go.SetActive(active);

            foreach (Transform child in go.transform)
            {
                SetActiveRecursive(child.gameObject, active);
            }
        }

        public static void SetLayerRecursively(this GameObject go, int layer)
        {
            go.layer = layer;

            foreach (Transform child in go.transform)
            { 
                SetLayerRecursively(child.gameObject, layer);
            }

        }
    }
}