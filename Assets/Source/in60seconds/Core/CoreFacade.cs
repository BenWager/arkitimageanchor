﻿/************************************************************************************
 * 																					*
 * CoreFacade.cs																    *
 * 																					*
 * This is the main entry point to the pureMVC framework.							*
 * 																					*
 ************************************************************************************/
namespace in60seconds.Core
{
	using PureMVC.Patterns;
	using PureMVC.Interfaces;
	using in60seconds.Core.Controller.Commands;
	using in60seconds.Core.Controller.Notes;
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;

	public class CoreFacade : Facade
	{
		public const string DEFAULT_INSTANCE_KEY = "Core";

		private string instanceKey;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:INDG.Core.CoreFacade"/> class.
		/// </summary>
		/// <param name="aKey">A key.</param>
		public CoreFacade(string aKey)
			: base(aKey)
		{
			instanceKey = aKey;

            DebugLogger.Log("CoreFacade::__Construct -> {0}", instanceKey);
        }

        /// <summary>
        /// Gets the default instance.
        /// </summary>
        /// <returns>The instance.</returns>
        new public static IFacade GetInstance()
		{
			return GetInstance(DEFAULT_INSTANCE_KEY);
		}

		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <returns>The instance.</returns>
		/// <param name="key">Key.</param>
		new public static IFacade GetInstance(string key)
		{
			if (!m_instanceMap.ContainsKey(key))
				return new CoreFacade(key);

			return m_instanceMap[key];
		}

		/// <summary>
		/// Initializes the controller.
		/// </summary>
		protected override void InitializeController()
		{
			base.InitializeController();

            // Application Startup command is a Bootstrapper that initiates everything
			RegisterCommand(CoreNote.CORE_START, typeof(CoreStartCommand));
		}

        /// <summary>
        /// Startup the specified coreBehaviour.
        /// </summary>
        /// <param name="coreBehaviour">Core behaviour.</param>
        public void Startup(CoreBehaviour coreBehaviour)
		{
            // Send Application start notification
            // This executes the CoreStartupCommand, which in turn registers all other mediators / commands and proxies
			SendNotification(CoreNote.CORE_START, coreBehaviour);
		}
	}
}