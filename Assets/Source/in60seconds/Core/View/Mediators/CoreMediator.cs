﻿namespace in60seconds.Core.View.Mediators
{
    using PureMVC.Interfaces;
    using System.Collections.Generic;
    using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
    using in60seconds.Core.Controller.Notes;
    using in60seconds.Core.Model.VO;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using System.Collections;

    public sealed class CoreMediator : CoreBaseMediator
    {
        /// <summary>
        /// The name of this mediator - for use internally in PureMVC
        /// </summary>
        new public const string NAME = "CoreMediator";

        /***********************************************************************************************************/

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Core.View.Mediators.CoreMediator"/> class.
        /// </summary>
        public CoreMediator(CoreBehaviour coreBehaviour) : base(NAME)
        {
            m_viewComponent = coreBehaviour;
        }

        /// <summary>
        /// On register.
        /// </summary>
        public override void OnRegister()
        {
            DebugLogger.Log(NAME + "::OnRegister");

            // Check if the application contains an EventSystem
            CheckEventSystemExists();

            // Listen for core reset event
            (m_viewComponent as CoreBehaviour).OnCoreReset += OnRequestCoreReset;
        }

        /// <summary>
        /// Gets the list notification interests.
        /// </summary>
        /// <value>The list notification interests.</value>
        public override IEnumerable<string> ListNotificationInterests
        {
            get
            {
                return new List<string>(base.ListNotificationInterests)
                {
                    CoreNote.REQUEST_START_COROUTINE,
                    CoreNote.REQUEST_STOP_COROUTINE,
                    CoreNote.REQUEST_STOP_ALL_COROUTINES
                };
            }
        }

        /// <summary>
        /// Handles the notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public override void HandleNotification(INotification notification)
        {
            switch (notification.Name)
            {
                case CoreNote.REQUEST_START_COROUTINE:
                    OnRequestStartCoroutine(notification.Body as RequestStartCoroutineVO);

                    break;
                case CoreNote.REQUEST_STOP_COROUTINE:
                    OnRequestStopCoroutine(notification.Body as IEnumerator);

                    break;

                case CoreNote.REQUEST_STOP_ALL_COROUTINES:
                    OnRequestStopAllCoroutines();

                    break;

                default: break;
            }
        }

        /// <summary>
        /// Attempts to start a coroutine
        /// </summary>
        /// <param name="requestStartCoroutineVO"></param>
        private void OnRequestStartCoroutine(RequestStartCoroutineVO requestStartCoroutineVO)
        {
            (m_viewComponent as CoreBehaviour).StartCoroutine(requestStartCoroutineVO.coroutine);
        }

        /// <summary>
        /// Attempts to stop a coroutine
        /// </summary>
        /// <param name="requestStartCoroutineVO"></param>
        private void OnRequestStopCoroutine(IEnumerator coroutine)
        {
            (m_viewComponent as CoreBehaviour).StopCoroutine(coroutine);
        }

        /// <summary>
        /// Attempts to stop all coroutines
        /// </summary>
        private void OnRequestStopAllCoroutines()
        {
            (m_viewComponent as CoreBehaviour).StopAllCoroutines();
        }

        /// <summary>
        /// Checks if an EventSystem exists in heirarchy, if none found, we add one
        /// </summary>
        private void CheckEventSystemExists()
        {
            // Check if we have an EventSystem already
            if (GameObject.FindObjectOfType<EventSystem>() == null)
            {
                GameObject eventSystem = new GameObject("EventSystem");
                eventSystem.AddComponent<EventSystem>();
                eventSystem.AddComponent<StandaloneInputModule>();
            }
        }
    }
}