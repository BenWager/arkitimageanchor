﻿namespace in60seconds.Core.View.Mediators
{
    using PureMVC.Patterns;
    using in60seconds.Core.Controller.Notes;
    using PureMVC.Interfaces;
    using System.Collections.Generic;
    using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
    using Model.Proxies;
    using UnityEngine;
    using in60seconds.Core.Model.VO;

    public class CoreBaseMediator : Mediator
    {
        /// <summary>
        /// The name of this mediator - for use internally in PureMVC
        /// </summary>
        new public const string NAME = "CoreBaseMediator";

        protected CoreDataProxy coreDataProxy;

        /***********************************************************************************************************/

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Core.View.Mediators.CoreBaseMediator"/> class.
        /// </summary>
        public CoreBaseMediator(string NAME):base(NAME) { }

        /// <summary>
        /// On register.
        /// </summary>
        public override void OnRegister()
        {
            DebugLogger.Log(NAME + "::OnRegister");

            coreDataProxy = Facade.RetrieveProxy(CoreDataProxy.NAME) as CoreDataProxy;
        }

        /// <summary>
        /// Gets the list notification interests.
        /// </summary>
        /// <value>The list notification interests.</value>
        public override IEnumerable<string> ListNotificationInterests
        {
            get
            {
                return new List<string>
                {
                    CoreNote.CORE_READY,
                    CoreNote.ENVIRONMENT_LOADED,
                    CoreNote.REQUEST_UPDATE_APPLICAITON_TIMESCALE
                };
            }
        }

        /// <summary>
        /// Handles the notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public override void HandleNotification(INotification notification)
        {
            switch (notification.Name)
            {
                case CoreNote.CORE_READY:
                    OnCoreReady();
                    break;

                case CoreNote.ENVIRONMENT_LOADED:
                    OnEnvironmentLoaded();
                    break;

                case CoreNote.REQUEST_UPDATE_APPLICAITON_TIMESCALE:
                    OnRequestUpdateApplicationTimeScale(notification.Body as RequestUpdateApplicationTimeScaleVO );
                    break;
            }
        }

        /// <summary>
        /// Called on application init, after PureMVC has been initiated
        /// </summary>
        protected virtual void OnCoreReady()
        {

        }

        /// <summary>
        /// Setup this instance.
        /// </summary>
        protected virtual void OnEnvironmentLoaded()
        {

        }

        /// <summary>
        /// Ons the application reset.
        /// </summary>
        protected virtual void OnRequestCoreReset()
        {
            SendNotification(CoreNote.REQUEST_CORE_RESET);
        }

        protected virtual void OnRequestUpdateApplicationTimeScale(RequestUpdateApplicationTimeScaleVO requestUpdateApplicationTimeScaleVO)
        {

        }
    }
}