﻿using System.Collections;
using UnityEngine.Events;

namespace in60seconds.Core.Model.VO
{
    public enum FadeDirection
    {
        IN,
        OUT
    }

	public class RequestBlackoutVO
	{
        public FadeDirection fadeDirection { get; set; }
		public UnityAction callback { get; set; }
		public object args { get; set; }
        public float delay { get; set; }
    }
}