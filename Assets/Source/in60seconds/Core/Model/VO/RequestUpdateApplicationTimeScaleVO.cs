﻿namespace in60seconds.Core.Model.VO
{
	public class RequestUpdateApplicationTimeScaleVO
	{
        public float timeScale { get; set; }
    }
}