﻿namespace in60seconds.Core.Model.VO
{
    public enum BlackoutStatus
    {
        NONE,
        IN,
        OUT
    }

	public class CoreDataVO
	{
		public string version { get; set; }
		public string currentEnvironment { get; set; }
        public string defaultEnvironment { get; set; }
        public bool isDebug { get; set; }
        public BlackoutStatus blackoutStatus { get; set; }
    }
}