﻿using System.Collections;

namespace in60seconds.Core.Model.VO
{
	public class RequestStartCoroutineVO
	{
		public IEnumerator coroutine { get; set; }
		public object args { get; set; }
    }
}