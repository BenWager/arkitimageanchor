﻿namespace in60seconds.Core.Model.Proxies
{
	using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
    using in60seconds.Core.Model.VO;
    using PureMVC.Patterns;
    using UnityEngine;

    public class CoreDataProxy : Proxy
	{
		new public const string NAME = "CoreDataProxy";

		protected CoreDataVO dataVO { get { return Data as CoreDataVO; } }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:INDG.Core.Model.Proxies.DataProxy"/> class.
		/// Create a new DataVO object
		/// Call method to set ay DataVO default values
		/// </summary>
		public CoreDataProxy() : base(NAME)
		{
			DebugLogger.Log(NAME + "::__Contstruct");

			Data = new CoreDataVO();

			SetDefaultValues();
		}

		/*************************************************************************************************/

		/// <summary>
		/// Sets the default values of DataVO
		/// </summary>
		private void SetDefaultValues()
		{
			dataVO.version = "1.0.0";
			dataVO.currentEnvironment = "";
			dataVO.defaultEnvironment = "Environment";

            // Search for a GameObject with CoreBehaviour componenent attached
            CoreBehaviour coreBehaviour = GameObject.FindObjectOfType<CoreBehaviour>();
            if(coreBehaviour != null)
            {
                dataVO.defaultEnvironment = coreBehaviour.DefaultEnvironmentScene;
                dataVO.isDebug = coreBehaviour.debug;
            }
        }

        /*************************************************************************************************/

        /// <summary>
        /// Gets the current environment.
        /// </summary>
        /// <returns>The current environment.</returns>
        public string GetCurrentEnvironment ()
        {
            return dataVO.currentEnvironment;
		}

		/// <summary>
		/// Sets the current environment
		/// </summary>
		/// <param name="environmentName">Environment name.</param>
		public void SetCurrentEnvironment(string environmentName)
		{
			dataVO.currentEnvironment = environmentName;
		}

		/// <summary>
		/// Gets the default environment. Used on Application reset
		/// </summary>
		/// <returns>The default environment.</returns>
		public string GetDefaultEnvironment()
		{
			return dataVO.defaultEnvironment;
		}

        public bool IsDebug()
        {
            return dataVO.isDebug;
        }

        public BlackoutStatus GetBlackoutStatus()
        {
            return dataVO.blackoutStatus;
        }

        public void SetBlackoutStatus(BlackoutStatus value)
        {
            dataVO.blackoutStatus = value;
        }
    }
}