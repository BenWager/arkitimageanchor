﻿using in60seconds.Application.Model.Proxies;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Application.Controller.Commands.Prepare
{
    public class ApplicationModelPrepareCommand : SimpleCommand
    {
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("ApplicationModelPrepareCommand::Execute");

            Facade.RegisterProxy(new ApplicationDataProxy());
            Facade.RegisterProxy(new ApplicationStateProxy());
        }
    }
}