﻿using in60seconds.Application.View.Mediators;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Application.Controller.Commands.Prepare
{
    public class ApplicationViewPrepareCommand : SimpleCommand
    {
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("ApplicationViewPrepareCommand::Execute");

            Facade.RegisterMediator(new ApplicationMediator());
            //Facade.RegisterMediator(new CameraMediator());

            Facade.RegisterMediator(new ARMediator());
        }
    }
}