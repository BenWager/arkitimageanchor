﻿using in60seconds.Application.Controller.Commands.Request;
using in60seconds.Application.Controller.Commands.Result;
using in60seconds.Application.Controller.Notes;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.ARSystem.Controller.Commands;
using in60seconds.Modules.CameraSystem.Controller.Commands;
using in60seconds.Modules.DataSystem.Controller.Commands;
using in60seconds.Modules.DataSystem.Controller.Notes;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Application.Controller.Commands.Prepare
{
    public class ApplicationControllerPrepareCommand : MacroCommand
    {

        protected override void InitializeMacroCommand()
        {
            DebugLogger.Log("ApplicationControllerPrepareCommand::InitializeMacroCommand");

            base.InitializeMacroCommand();

            AddSubCommand(typeof(PrepareControllers));

            // Add Modules support
            AddSubCommand(typeof(ARSystemPrepareCommand));
            //AddSubCommand(typeof(CameraSystemPrepareCommand));
            //AddSubCommand(typeof(DataSystemPrepareCommand));

        }
    }

    public class PrepareControllers : SimpleCommand
    {
        public override void Execute(INotification notification)
        {

            // Register Requests
            Facade.RegisterCommand(ApplicationNote.REQUEST_LOAD_APPLICATION_DATA, typeof(RequestLoadApplicationDataCommand));
            Facade.RegisterCommand(ApplicationNote.REQUEST_APPLICATION_STATE_CHANGE, typeof(RequestApplicationStateChangeCommand));

            // Register Results
            Facade.RegisterCommand(DataSystemNote.DATA_LOADED, typeof(ApplicationDataLoadedCommand));
        }
    }
}