﻿using in60seconds.Application.Controller.Notes;
using in60seconds.Application.Model.Proxies;
using in60seconds.Application.Model.VO;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Application.Controller.Commands.Request
{
    public class RequestApplicationStateChangeCommand : SimpleCommand
    {
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("RequestApplicationStateChangeCommand::Execute");

            ApplicationStateProxy applicationStateProxy = Facade.RetrieveProxy(ApplicationStateProxy.NAME) as ApplicationStateProxy;

            ApplicationStateVO applicationStateVO = notification.Body as ApplicationStateVO;

            applicationStateVO.previousState = applicationStateProxy.GetCurrentState();

            applicationStateProxy.SetCurrentState(applicationStateVO);

            SendNotification(ApplicationNote.APPLICATION_STATE_CHANGED, applicationStateVO);
        }
    }
}