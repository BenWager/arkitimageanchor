﻿/***************************************************
 * 
 * A request to handle processing application data
 * via the DataSystem module
 * 
 * Responds to ApplicationNote.REQUEST_LOAD_APPLICATION_DATA
 * 
 ***************************************************/

using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.DataSystem.Controller.Notes;
using in60seconds.Modules.DataSystem.Model.VO;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Application.Controller.Commands.Request
{
    public class RequestLoadApplicationDataCommand : SimpleCommand
    {
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("RequestLoadApplicationDataCommand::Execute");

            RequestLoadDataVO requestLoadDataVO = new RequestLoadDataVO();
            requestLoadDataVO.requestType = RequestLoadDataType.RESOURCES;
            requestLoadDataVO.path = "Data/USPs.json";
            requestLoadDataVO.dataType = typeof(in60seconds.Application.Model.VO.ApplicationDataVO);

            SendNotification(DataSystemNote.REQUEST_LOAD_DATA, requestLoadDataVO);
        }
    }
}
