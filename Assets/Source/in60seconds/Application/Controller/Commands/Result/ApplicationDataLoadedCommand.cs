﻿using in60seconds.Application.Controller.Notes;
using in60seconds.Application.Model.Proxies;
using in60seconds.Application.Model.VO;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace in60seconds.Application.Controller.Commands.Result
{
    public class ApplicationDataLoadedCommand : SimpleCommand
    {
        public override void Execute(INotification notification)
        {
            DebugLogger.Log("ApplicationDataLoadedCommand::Execute");

            // Check data type
            if ((notification.Body as ApplicationDataVO) != null)
            {
                (Facade.RetrieveProxy(ApplicationDataProxy.NAME) as ApplicationDataProxy).Data = notification.Body as ApplicationDataVO;

                // Data is loaded. Application is now ready!
                SendNotification(ApplicationNote.APPLICATION_READY);
            }
        }
    }
}