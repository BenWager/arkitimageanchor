﻿namespace in60seconds.Application.Controller.Notes
{
    public class ApplicationNote
    {
        public const string REQUEST_LOAD_APPLICATION_DATA = "application_requestLoadApplicationData";
        public const string APPLICATION_READY = "application_applicationReady";

        public const string REQUEST_APPLICATION_STATE_CHANGE = "application_requestApplicationStateChange";
        public const string APPLICATION_STATE_CHANGED = "application_applicationStateChanged";
    }
}