﻿using UnityEngine;
using System.Collections;
using PureMVC.Patterns;
using in60seconds.Modules.ARSystem.Controller.Notes;
using in60seconds.Modules.ARSystem.Model.VO;

public class ARTrackedModelComponent : MonoBehaviour
{
    private Vector3 normalScale = Vector3.zero;
    public ARImageAnchorVO aRImageAnchorVO;

    public void Show()
    {
        Debug.Log("SHOW!");
        gameObject.SetActive(true);

        StopAllCoroutines();
        StartCoroutine(DoShow());
    }

    public IEnumerator DoShow()
    {
        if (normalScale.x == 0)
        {
            Debug.Log("Adjust Normal Scale");
            yield return new WaitForEndOfFrame();
            normalScale = transform.localScale;
        }


        Debug.Log(normalScale);
        Vector3 startScale = Vector3.zero;
        Vector3 endScale = normalScale;
        float time = .6f;
        float elapsed = 0f;

        while(elapsed < time)
        {
            float scale = EaseOutBounce(startScale.x, endScale.x, elapsed / time);
            transform.localScale = new Vector3(scale, scale, scale);
            elapsed += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.localScale = endScale;
    }

    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine(DoHide());
    }

    public IEnumerator DoHide()
    {
        Vector3 startScale = transform.localScale;
        Vector3 endScale = Vector3.zero;
        float time = .6f;
        float elapsed = 0f;

        while (elapsed < time)
        {
            float scale = EaseInOutBounce(startScale.x, endScale.x, elapsed / time);
            transform.localScale = new Vector3(scale, scale, scale);
            elapsed += Time.deltaTime;


            yield return new WaitForEndOfFrame();
        }

        transform.localScale = endScale;

        gameObject.SetActive(false);
    }

    void OnBecameInvisible()
    {
        Facade.GetInstance().SendNotification(ARSystemNote.AR_IMAGE_ANCHOR_LOST, aRImageAnchorVO);
    }


    /////////////////////////////////////////////////////////////////////////////
    /// Easing methods for model reveal
    /////////////////////////////////////////////////////////////////////////////

    public  float EaseInBounce(float start, float end, float value)
    {
        end -= start;
        float d = 1f;
        return end - EaseOutBounce(0, end, d - value) + start;
    }

    public  float EaseOutBounce(float start, float end, float value)
    {
        value /= 1f;
        end -= start;
        if (value < (1 / 2.75f))
        {
            return end * (7.5625f * value * value) + start;
        }
        else if (value < (2 / 2.75f))
        {
            value -= (1.5f / 2.75f);
            return end * (7.5625f * (value) * value + .75f) + start;
        }
        else if (value < (2.5 / 2.75))
        {
            value -= (2.25f / 2.75f);
            return end * (7.5625f * (value) * value + .9375f) + start;
        }
        else
        {
            value -= (2.625f / 2.75f);
            return end * (7.5625f * (value) * value + .984375f) + start;
        }
    }

    public  float EaseInOutBounce(float start, float end, float value)
    {
        end -= start;
        float d = 1f;
        if (value < d * 0.5f) return EaseInBounce(0, end, value * 2) * 0.5f + start;
        else return EaseOutBounce(0, end, value * 2 - d) * 0.5f + end * 0.5f + start;
    }
}
