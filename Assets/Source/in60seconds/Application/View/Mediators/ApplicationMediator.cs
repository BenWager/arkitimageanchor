﻿//
// This mediator is used to create 
//

using in60seconds.Application.Controller.Notes;
using in60seconds.Application.Model.Enums;
using in60seconds.Application.Model.Proxies;
using in60seconds.Application.Model.VO;
using in60seconds.Core.Controller.Notes;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Core.Model.VO;
using in60seconds.Core.View.Mediators;
using PureMVC.Interfaces;
using System.Collections.Generic;

namespace in60seconds.Application.View.Mediators
{
    public class ApplicationMediator : CoreBaseMediator
    {
        new public const string NAME = "ApplicationMediator";

        // Flag to track when scene is loading 
        private bool isSceneLoading = false;

        // Store references to application data and state proxies
        protected ApplicationDataProxy applicationDataProxy;
        protected ApplicationStateProxy applicationStateProxy;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Application.View.Mediators.ApplicationMediator"/> class.
        /// </summary>
        public ApplicationMediator() : base(NAME) { }
        
        /// <summary>
        /// Gets the list notification interests.
        /// </summary>
        /// <value>The list notification interests.</value>
        public override IEnumerable<string> ListNotificationInterests
        {
            get
            {
                return new List<string>(base.ListNotificationInterests)
                {
                    ApplicationNote.APPLICATION_READY,
                    ApplicationNote.APPLICATION_STATE_CHANGED,
                };
            }
        }

        /// <summary>
        /// Handles the notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public override void HandleNotification(INotification notification)
        {
            base.HandleNotification(notification);

            switch (notification.Name)
            {
                case ApplicationNote.APPLICATION_READY:
                    OnApplicationReady();

                    break;

                case ApplicationNote.APPLICATION_STATE_CHANGED:
                    OnApplicationStateChanged(notification.Body as ApplicationStateVO);

                    break;

                default: break;
            }
        }

        /// <summary>
        /// Core Framework is ready
        /// </summary>
        protected override void OnCoreReady()
        {
            base.OnCoreReady();

            // If application requires the processing of a data file, call this...
            //SendNotification(ApplicationNote.REQUEST_LOAD_APPLICATION_DATA);

            // Otherwise we are ready
            SendNotification(ApplicationNote.APPLICATION_READY);
        }

        /// <summary>
        /// Called once application data has been loaded
        /// </summary>
        protected void OnApplicationReady()
        {
            DebugLogger.Log(NAME + "::OnApplicationReady");

            // Set reference to data and state proxies
            applicationDataProxy = Facade.RetrieveProxy(ApplicationDataProxy.NAME) as ApplicationDataProxy;
            applicationStateProxy = Facade.RetrieveProxy(ApplicationStateProxy.NAME) as ApplicationStateProxy;

            // Request change application state to HOME
            ApplicationStateVO newApplicationStateVO = new ApplicationStateVO();
            newApplicationStateVO.state = ApplicationState.HOME;
            SendNotification(ApplicationNote.REQUEST_APPLICATION_STATE_CHANGE, newApplicationStateVO);
        }

        /// <summary>
        /// Handle application state change
        /// </summary>
        /// <param name="applicationStateVO"></param>
        protected void OnApplicationStateChanged(ApplicationStateVO applicationStateVO)
        {
            switch (applicationStateVO.state)
            {
                case ApplicationState.HOME:

                break;
            }
        }

        /// <summary>
        /// Handle scene load
        /// </summary>
        protected override void OnEnvironmentLoaded()
        {
            DebugLogger.Log(NAME + "::OnEnvironmentLoaded");

            isSceneLoading = false;

            // Request blackout fade out
            RequestBlackoutVO requestBlackoutVO = new RequestBlackoutVO();
            requestBlackoutVO.fadeDirection = FadeDirection.OUT;
            SendNotification(CoreNote.REQUEST_BLACKOUT, requestBlackoutVO);
        }
    }
}