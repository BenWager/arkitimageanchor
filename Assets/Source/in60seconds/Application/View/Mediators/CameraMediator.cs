﻿//
// This mediator is used to create 
//

using in60seconds.Application.Controller.Notes;
using in60seconds.Application.Model.Enums;
using in60seconds.Application.Model.VO;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Core.View.Mediators;
using in60seconds.Modules.CameraSystem;
using in60seconds.Modules.CameraSystem.Controller.Notes;
using in60seconds.Modules.CameraSystem.Model.VO;
using PureMVC.Interfaces;
using System.Collections.Generic;
using UnityEngine;

namespace in60seconds.Application.View.Mediators
{

    public class CameraMediator : CoreBaseMediator
    {
        new public const string NAME = "CameraMediator";

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Application.View.Mediators.CameraSystemMediator"/> class.
        /// </summary>
        public CameraMediator() : base(NAME) { }
        
        /// <summary>
        /// Gets the list notification interests.
        /// </summary>
        /// <value>The list notification interests.</value>
        public override IEnumerable<string> ListNotificationInterests
        {
            get
            {
                return new List<string>(base.ListNotificationInterests)
                {
                    ApplicationNote.APPLICATION_READY,
                    ApplicationNote.APPLICATION_STATE_CHANGED,
                };
            }
        }

        /// <summary>
        /// Handles the notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public override void HandleNotification(INotification notification)
        {
            base.HandleNotification(notification);

            switch (notification.Name)
            {
                case ApplicationNote.APPLICATION_READY:
                    OnApplicationReady();

                    break;

                case ApplicationNote.APPLICATION_STATE_CHANGED:
                    OnApplicationStateChanged(notification.Body as ApplicationStateVO);

                    break;

                default: break;
            }
        }

        /// <summary>
        /// Called once application data has been loaded
        /// </summary>
        protected void OnApplicationReady()
        {
            DebugLogger.Log(NAME + "::OnApplicationReady");

            InitCameras();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void InitCameras()
        {
            DebugLogger.Log(NAME + "::InitCameras");

            // Main Camera
            GameObject mainCameraGameObject = new GameObject("Main");
            Camera mainCamera = mainCameraGameObject.AddComponent<Camera>();
            mainCamera.clearFlags = CameraClearFlags.SolidColor;
            mainCamera.cullingMask = LayerMask.GetMask("Default");
            mainCamera.backgroundColor = new Color32(224, 225, 229, 255);
            mainCamera.nearClipPlane = 0.01f;
            mainCamera.depth = 0;

            // UI Camera
            GameObject UICameraGameObject = new GameObject("UI");
            Camera UICamera = UICameraGameObject.AddComponent<Camera>();
            UICamera.cullingMask = LayerMask.GetMask("UI");
            UICamera.clearFlags = CameraClearFlags.Depth;
            UICamera.nearClipPlane = 0.01f;
            UICamera.depth = 2;

            SendNotification(CameraSystemNote.REQUEST_ADD_CAMERA, mainCameraGameObject);
            SendNotification(CameraSystemNote.REQUEST_ADD_CAMERA, UICameraGameObject);

            // Set an initial camera position
            CameraSystemTransitionVO cameraSystemTransitionVO = new CameraSystemTransitionVO();
            cameraSystemTransitionVO.easing = Modules.CameraSystem.Model.Enums.CameraSystemEasing.MOVE;
            GameObject destination = new GameObject();
            destination.transform.position = new Vector3(0, 0, -5);
            cameraSystemTransitionVO.destination = destination.transform;
            SendNotification(CameraSystemNote.REQUEST_MOVE_CAMERA, cameraSystemTransitionVO);
            GameObject.Destroy(destination);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationStateVO"></param>
        protected void OnApplicationStateChanged(ApplicationStateVO applicationStateVO)
        {
            switch (applicationStateVO.state)
            {
                case ApplicationState.HOME:

                break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnEnvironmentLoaded()
        {
            DebugLogger.Log(NAME + "::OnEnvironmentLoaded");

        }
    }
}