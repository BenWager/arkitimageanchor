﻿//
// This mediator is used to create 
//

using in60seconds.Application.Controller.Notes;
using in60seconds.Application.Model.Enums;
using in60seconds.Application.Model.Proxies;
using in60seconds.Application.Model.VO;
using in60seconds.Core.Controller.Notes;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Core.Model.VO;
using in60seconds.Core.View.Mediators;
using in60seconds.Modules.ARSystem.Controller.Notes;
using in60seconds.Modules.ARSystem.Model.VO;
using PureMVC.Interfaces;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace in60seconds.Application.View.Mediators
{
    public class ARMediator : CoreBaseMediator
    {
        new public const string NAME = "ARMediator";


        // Store references to application data and state proxies
        protected ApplicationDataProxy applicationDataProxy;
        protected ApplicationStateProxy applicationStateProxy;
        protected Dictionary<string, ARImageAnchorVO> ARImageAnchors = new Dictionary<string, ARImageAnchorVO>();

        //
        // This should be separated in to a component class
        //
        private GameObject FindMarker;

        // Buttons container
        private GameObject ButtonsContainer;

        // Track current image anchor
        private ARImageAnchorVO currentImageAnchorVO;
        private ARReferenceImagesSet aRReferenceImagesSet;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Application.View.Mediators.ApplicationMediator"/> class.
        /// </summary>
        public ARMediator() : base(NAME) { }

        /// <summary>
        /// Gets the list notification interests.
        /// </summary>
        /// <value>The list notification interests.</value>
        public override IEnumerable<string> ListNotificationInterests
        {
            get
            {
                return new List<string>(base.ListNotificationInterests)
                {
                    ApplicationNote.APPLICATION_READY,
                    ApplicationNote.APPLICATION_STATE_CHANGED,

                    ARSystemNote.AR_IMAGE_ANCHOR_DETECTED,
                    ARSystemNote.AR_IMAGE_ANCHOR_UPDATED,
                    ARSystemNote.AR_IMAGE_ANCHOR_LOST
                };
            }
        }

        /// <summary>
        /// Handles the notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public override void HandleNotification(INotification notification)
        {
            base.HandleNotification(notification);

            switch (notification.Name)
            {
                case ApplicationNote.APPLICATION_READY:
                    OnApplicationReady();

                    break;

                case ApplicationNote.APPLICATION_STATE_CHANGED:
                    OnApplicationStateChanged(notification.Body as ApplicationStateVO);

                    break;

                case ARSystemNote.AR_IMAGE_ANCHOR_DETECTED:
                    OnARImageAnchorDetected(notification.Body as ARImageAnchorVO);

                    break;

                case ARSystemNote.AR_IMAGE_ANCHOR_UPDATED:
                    OnARImageAnchorUpdated(notification.Body as ARImageAnchorVO);

                    break;

                case ARSystemNote.AR_IMAGE_ANCHOR_LOST:
                    OnARImageAnchorLost(notification.Body as ARImageAnchorVO);

                    break;

                default: break;
            }
        }

        /// <summary>
        /// Core Framework is ready
        /// </summary>
        protected override void OnCoreReady()
        {
            base.OnCoreReady();
        }

        /// <summary>
        /// Called once application data has been loaded
        /// </summary>
        protected void OnApplicationReady()
        {
            DebugLogger.Log(NAME + "::OnApplicationReady");

            // Set reference to data and state proxies
            applicationDataProxy = Facade.RetrieveProxy(ApplicationDataProxy.NAME) as ApplicationDataProxy;
            applicationStateProxy = Facade.RetrieveProxy(ApplicationStateProxy.NAME) as ApplicationStateProxy;

            // Request an ARCamera
            SendNotification(ARSystemNote.REQUEST_AR_CAMERA);

            // Request an ARLight
            SendNotification(ARSystemNote.REQUEST_AR_LIGHT);


            // Get UI Elements
            FindMarker = GameObject.Find("Canvas/FindMarker");
            ButtonsContainer = GameObject.Find("Canvas/ButtonsScroller/Viewport/Content");
            ButtonsContainer.SetActive(false);

            // webview setup
            UniWebView.SetAllowAutoPlay(true);
            UniWebView.SetAllowInlinePlay(true);

            // Store the images set
            // This contains prefab and url info for ARCore && ARKit
            aRReferenceImagesSet = Resources.Load("ARSystem/ARKitReferenceImagesSet") as ARReferenceImagesSet;

            // Load the reference images set from Resources
            LoadImageSet();

        }

#if UNITY_IOS

        private void LoadImageSet()
        {
            // Request add ARReferenceImageSet
            SendNotification(ARSystemNote.REQUEST_ADD_ARKIT_REF_IMAGE_SET, aRReferenceImagesSet);
        }

#elif UNITY_ANDROID

        private void LoadImageSet()
        {
            // Load the ARCore image reference set
            //ARReferenceImagesSet referenceImagesSet = Resources.Load("ARSystem/ARKitReferenceImagesSet") as ARReferenceImagesSet;

            // Request add ARReferenceImageSet
            //SendNotification(ARSystemNote.REQUEST_ADD_ARKIT_REF_IMAGE_SET, referenceImagesSet);

        }

#else
        private void LoadImageSet()
        {
            // No ImageSet to load
        }
#endif

        private void OnLaunchButtonClick(string url)
        {
            Debug.Log("Launch Click");

            GameObject webViewGameObject = new GameObject("UniWebView");

            UniWebView uniWebView = webViewGameObject.AddComponent<UniWebView>();

            if (!url.Contains("http"))
            {
                uniWebView.Load(UniWebViewHelper.StreamingAssetURLForPath(url));
            }
            else
            {
                uniWebView.Load(url);
            }

            //uniWebView.ReferenceRectTransform = GameObject.Find("WebViewRect").GetComponent<RectTransform>();

            uniWebView.Frame = new Rect(0, 0, Screen.width, Screen.height);

            uniWebView.SetShowToolbar(false);
            uniWebView.SetShowSpinnerWhileLoading(true);
            uniWebView.SetUseWideViewPort(true);
            uniWebView.SetImmersiveModeEnabled(false);
            uniWebView.OnMessageReceived += (view, message) =>
            {
                if (message.Path.Equals("close"))
                {
                    GameObject.Destroy(uniWebView);

                    uniWebView = null;

                    GameObject.Destroy(webViewGameObject);

                    KillARTracking();
                }
            };


            uniWebView.OnShouldClose += (view) =>
            {
                uniWebView = null;

                GameObject.Destroy(webViewGameObject);
                return true;
            };


            uniWebView.OnOrientationChanged += (view, orientation) => {
                uniWebView.Frame = new Rect(0, 0, Screen.width, Screen.height);
            };

            uniWebView.Show();

            uniWebView.Frame = new Rect(0, 0, Screen.width, Screen.height);
        }

#if UNITY_ANDROID
        private void KillARTracking()
        {
            SendNotification(ARSystemNote.AR_IMAGE_ANCHOR_LOST, currentImageAnchorVO);
        }
#else
        private void KillARTracking()
        {

        }
#endif

        /// <summary>
        /// Handle application state change
        /// </summary>
        /// <param name="applicationStateVO"></param>
        protected void OnApplicationStateChanged(ApplicationStateVO applicationStateVO)
        {
            switch (applicationStateVO.state)
            {
                case ApplicationState.HOME:

                break;
            }
        }

        private void OnARImageAnchorDetected(ARImageAnchorVO aRImageAnchorVO)
        {
            if(ARImageAnchors.ContainsKey(aRImageAnchorVO.identifier))
            {
                // Check if instance is visible
                if(!ARImageAnchors[aRImageAnchorVO.identifier].gameObject.activeSelf)
                {
                    ARImageAnchors[aRImageAnchorVO.identifier].gameObject.transform.SetPositionAndRotation(aRImageAnchorVO.position,
                                                                                                           aRImageAnchorVO.rotation);

                    ARImageAnchors[aRImageAnchorVO.identifier].gameObject.GetComponent<ARTrackedModelComponent>().Show();
                }
            }
            else
            {
                Debug.Log("Find prefab data");
                foreach(ARReferenceImage image in aRReferenceImagesSet.referenceImages)
                {
                    Debug.Log("Checking image.imageName : " + aRImageAnchorVO.name);
                    if(image.imageName == aRImageAnchorVO.name)
                    {
                        aRImageAnchorVO.prefab = image.prefab;
                        aRImageAnchorVO.buttons = image.buttons;
                    }
                }
                // Create an instance
                aRImageAnchorVO.gameObject = GameObject.Instantiate(aRImageAnchorVO.prefab,
                                                                    aRImageAnchorVO.position,
                                                                    aRImageAnchorVO.rotation);
                
                ARImageAnchors.Add(aRImageAnchorVO.identifier, aRImageAnchorVO);

                ARTrackedModelComponent component = ARImageAnchors[aRImageAnchorVO.identifier].gameObject.AddComponent<ARTrackedModelComponent>();
                component.aRImageAnchorVO = aRImageAnchorVO;
                component.Show();
            }

            // Set our current imageanchor
            currentImageAnchorVO = ARImageAnchors[aRImageAnchorVO.identifier];

            // Hide the FIND MARKER object
            FindMarker.SetActive(false);


            // Create Buttons 
            foreach(Transform child in ButtonsContainer.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            foreach(URLButton button in currentImageAnchorVO.buttons)
            {
                GameObject go = GameObject.Instantiate(button.prefab, ButtonsContainer.transform);
                if(button.label != "" && go.GetComponentInChildren<Text>() != null)
                {
                    go.GetComponentInChildren<Text>().text = button.label;
                }
                string url = button.url;
                go.GetComponent<Button>().onClick.AddListener(() =>
                {
                    OnLaunchButtonClick(url);
                });
            }
            ButtonsContainer.SetActive(true);
        }

        private void OnARImageAnchorUpdated(ARImageAnchorVO aRImageAnchorVO)
        {
            Debug.Log("Update");
            if (ARImageAnchors.ContainsKey(aRImageAnchorVO.identifier))
            {
                if (aRImageAnchorVO.isTracked)
                {
                    // Check if instance is visible
                    if (!ARImageAnchors[aRImageAnchorVO.identifier].gameObject.activeSelf)
                    {
                        ARImageAnchors[aRImageAnchorVO.identifier].gameObject.GetComponent<ARTrackedModelComponent>().Show();
                        currentImageAnchorVO = ARImageAnchors[aRImageAnchorVO.identifier];
                        FindMarker.SetActive(false);

                        if (ButtonsContainer.transform.childCount <= 0)
                        {
                            foreach (URLButton button in currentImageAnchorVO.buttons)
                            {
                                GameObject go = GameObject.Instantiate(button.prefab, ButtonsContainer.transform);
                                if (button.label != "" && go.GetComponentInChildren<Text>() != null)
                                {
                                    go.GetComponentInChildren<Text>().text = button.label;
                                }
                                string url = button.url;
                                go.GetComponent<Button>().onClick.AddListener(() =>
                                {
                                    OnLaunchButtonClick(url);
                                });
                            }
                        }
                        ButtonsContainer.SetActive(true);


                    }

                    ARImageAnchors[aRImageAnchorVO.identifier].gameObject.transform.SetPositionAndRotation(aRImageAnchorVO.position,
                                                                                           aRImageAnchorVO.rotation);
                }
                else if(ARImageAnchors[aRImageAnchorVO.identifier].gameObject.activeSelf)
                {
                    ARImageAnchors[aRImageAnchorVO.identifier].gameObject.GetComponent<ARTrackedModelComponent>().Hide();

                    currentImageAnchorVO = null;
                    FindMarker.SetActive(true);

                    // Remove Buttons
                    ButtonsContainer.SetActive(false);
                    foreach (Transform child in ButtonsContainer.transform)
                    {
                        GameObject.Destroy(child.gameObject);
                    }

                }
            }
        }

        private void OnARImageAnchorLost(ARImageAnchorVO aRImageAnchorVO)
        {
            Debug.Log("Lost anchor");
            if (ARImageAnchors.ContainsKey(aRImageAnchorVO.identifier))
            {
                Debug.Log("Get VO");
                // Check if instance is visible
                if (!ARImageAnchors[aRImageAnchorVO.identifier].gameObject.activeSelf)
                {
                    Debug.Log("HIDE");
                    ARImageAnchors[aRImageAnchorVO.identifier].gameObject.GetComponent<ARTrackedModelComponent>().Hide();

                    currentImageAnchorVO = null;
                    FindMarker.SetActive(true);

                    // Remove Buttons
                    ButtonsContainer.SetActive(false);
                    foreach (Transform child in ButtonsContainer.transform)
                    {
                        GameObject.Destroy(child.gameObject);
                    }
                }
            }
        }
    }
}