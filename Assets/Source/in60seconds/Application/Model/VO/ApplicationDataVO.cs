﻿using in60seconds.Modules.DataSystem.Model.VO;
using UnityEngine;

namespace in60seconds.Application.Model.VO
{
    [System.Serializable]
    public class ApplicationDataVO:IDataSystemResult
    {
        // Outline the data structure here...
    }
}