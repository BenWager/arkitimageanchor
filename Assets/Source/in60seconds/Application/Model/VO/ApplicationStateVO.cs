﻿using in60seconds.Application.Model.Enums;

namespace in60seconds.Application.Model.VO
{
    public class ApplicationStateVO
    {
        public ApplicationStateVO previousState;
        public ApplicationState state;
    }
}