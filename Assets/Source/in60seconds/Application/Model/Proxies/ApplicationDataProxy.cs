﻿using in60seconds.Application.Model.VO;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using in60seconds.Modules.DataSystem.Controller.Notes;
using in60seconds.Modules.DataSystem.Model.VO;
using PureMVC.Patterns;

namespace in60seconds.Application.Model.Proxies
{
    public class ApplicationDataProxy : Proxy
    {
        new public const string NAME = "ApplicationDataProxy";

        internal ApplicationDataVO applicationDataVO { get { return Data as ApplicationDataVO; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Core.Model.Proxies.ApplicationDataProxy"/> class.
        /// Create a new DataVO object
        /// Call method to set ay DataVO default values
        /// </summary>
        public ApplicationDataProxy() : base(NAME)
		{
            DebugLogger.Log(NAME + "::__Contstruct");
            
        }
    }
}
