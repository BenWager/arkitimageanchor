﻿using in60seconds.Application.Model.VO;
using in60seconds.Core.Libraries.UnityLib.Utilities.Logging;
using PureMVC.Patterns;

namespace in60seconds.Application.Model.Proxies
{
    public class ApplicationStateProxy : Proxy
    {
        new public const string NAME = "ApplicationStateProxy";

        internal ApplicationStateVO applicationStateVO { get { return Data as ApplicationStateVO; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:INDG.Core.Model.Proxies.ApplicationStateProxy"/> class.
        /// Create a new DataVO object
        /// Call method to set ay DataVO default values
        /// </summary>
        public ApplicationStateProxy() : base(NAME)
		{
            DebugLogger.Log(NAME + "::__Contstruct");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationStateVO"></param>
        public void SetCurrentState(ApplicationStateVO applicationStateVO)
        {
            Data = applicationStateVO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ApplicationStateVO GetCurrentState()
        {
            return applicationStateVO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ApplicationStateVO GetPreviousState()
        {
            return applicationStateVO.previousState;
        }
    }
}
